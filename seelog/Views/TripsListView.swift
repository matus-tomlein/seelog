//
//  TripsListView.swift
//  seelog
//
//  Created by Matus Tomlein on 23/02/2020.
//  Copyright © 2020 Matus Tomlein. All rights reserved.
//

import SwiftUI

struct TripsListView: View {
    var destination: Trippable
    @EnvironmentObject var selectedYearState: SelectedYearState
    var trips: [Trip] { get { return destination.tripsForYear(selectedYearState.year) } }

    var body: some View {
        Section(header: Text("\(trips.count) trips")) {
            ForEach(trips) { trip in
                Text(trip.formatDateInterval())
            }
        }
    }
}

struct TripsListView_Previews: PreviewProvider {
    static var previews: some View {
        let model = simulatedDomainModel()
        
        return List {
            TripsListView(destination: model.countries[0])
        }
    }
}
